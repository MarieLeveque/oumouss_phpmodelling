program main

    implicit double precision (a-h, l-z)

    pi=acos(-1.)

!--------------param�tres thermo-physique acier maraging-----------------!
    c_acier=450                          !J/kgK
    rho_acier=8.0e3                      !kg/m3

    !m_bloc=0.2335+0.1802                !kg
    m_acier0=4.286                       !kg
    m_cond=rho_acier*(pi*6e-2**2*2e-2-pi*5e-2*pi*4e-3**2)!kg 2cm
    m_acier=m_acier0-m_cond              !kg masse "full cylinder"

    V_rmv=4.4e-2*pi*6.9e-2**2/4
    m_acier2=m_acier-rho_acier*V_rmv     !kg masse "hollow cylinder"

    print*,'m_acier_full =',m_acier,'kg'
    print*,'m_acier_hollow =',m_acier2,'kg'
    !print*,'m_cond =',m_cond,'kg'

!------------R�sistance thermique vers l'ambiance------------------------!

    d_php=8e-2                          !m
    h_php=4e-2                          !m
    S_iso=pi*d_php**2/4                 !m2
    e_iso=4e-3                          !m

    h_atm=10                            !W/m�K
    lambda_iso=0.04                     !W/mK

    R_iso=((lambda_iso*S_iso)/e_iso+2*pi*lambda_iso*h_php/log((d_php+2*e_iso)/d_php))**(-1)+1/(h_atm*S_iso)        !W/K
    print*,'R_isolant =',R_iso, 'K/W'

!-------------------param�tres conductance equivalente -----------------!

    aT=-0.000490811
    bT=0.0961139
    cT=-1.61031

    aG=0.0000130227
    bG=-0.00378628
    cG=0.370325
    dG=-9.10969



!----------------------Conditions initiales/limites --------------------!

    T_SF=10         !�C
    T_0=10          !�C
    T_ev=T_0        !�C
    T_ev2=T_0       !�C
    T_evG=T_0       !�C
    T_evG2=T_0      !�C
    T_evT=T_0       !�C
    T_evT2=T_0      !�C
    T_atm=30        !�C

    dt=1            !s pas de temps

!--------------------param�tres cycle puissance ------------------------!

    t1Q=0
    t2Q=1

!---------------------- type de cycle --------------------!

    print*,'cycle ? PS=0 ou GS=1'
    read*,Ncycle
    if (Ncycle == 0) then
        ktot=550*60
        Q1= 91
        Step=20
        iNS=12
        kperm=0.511*3600
    else !(Ncycle == 1) then
        ktot=260*60/2
        Q1=0
        Step=300
        iNS=2
        kperm=ktot/iNs
    end if

    open (unit=10,file='Sorties.out')

!-------------------------evolution transitoire-------------------------!

    do k=0,ktot

        treel=k*dt

!cycle de puissance

        if (Ncycle == 0) then
            Q_elec=Q1+(27)/(1+exp(-((treel-(3100-60*4)*dt-kperm*dt)-t1Q)/t2Q))&
            +(Q1+Step-Q1)/(1+exp(-((treel-(3100+1200)*dt-kperm*dt*2)-t1Q)/t2Q))&
            +(18)/(1+exp(-((treel-(3100+1200*2-70)*dt-kperm*dt*3)-t1Q)/t2Q))&
            +(Q1+Step-Q1)/(1+exp(-((treel-(3100*2+1200*2-70+60)*dt-kperm*dt*4)-t1Q)/t2Q))&
            +(Q1+Step-Q1)/(1+exp(-((treel-(3100*2+1200*2+60*3)*dt-kperm*dt*5)-t1Q)/t2Q))&
            +(19)/(1+exp(-((treel-(3100*2+1200*2+60*3.5)*dt-kperm*dt*6)-t1Q)/t2Q))&
            +(18)/(1+exp(-((treel-(3100*2+1200*2+60)*dt-kperm*dt*7)-t1Q)/t2Q))&
            +(Q1+Step-Q1)/(1+exp(-((treel-(3100*2+1200*2+60)*dt-kperm*dt*8)-t1Q)/t2Q))&
            +(Q1+Step-Q1)/(1+exp(-((treel-(3100*2+1200*2+40)*dt-kperm*dt*9)-t1Q)/t2Q))&
            +(19)/(1+exp(-((treel-(3100*2+1200*2+20)*dt-kperm*dt*10)-t1Q)/t2Q))&
            -(292)/(1+exp(-((treel-(3100*2+1200*2)*dt-kperm*dt*11)-t1Q)/t2Q))
        else !(Ncycle == 1) then
            Q_elec=Q1+Step/(1+exp(-((treel-(60*5)*dt))/t2Q))&
            -Step/(1+exp(-((treel-(kperm+60*5)*dt))/t2Q))
        endif

        Q_pertes=(T_evT-T_atm)/R_iso
        Q=Q_elec-Q_pertes


!conductance

        if (T_evT>66) then
            G_eqT=aG*T_evT**3+bG*T_evT**2+cG*T_evT+dG
       else
            G_eqT=1.75
        end if

        if (T_evT2>66) then
            G_eqT2=aG*T_evT2**3+bG*T_evT2**2+cG*T_evT2+dG
        else
            G_eqT2=G_eqT
        endif

!evolution de la temp�rature

		T_evT=T_evT+(Q/(m_acier*c_acier)-G_eqT/(m_acier*c_acier)*(T_evT-T_SF))*dt
        T_evT2=T_evT2+(Q/(m_acier2*c_acier)-G_eqT2/(m_acier2*c_acier)*(T_evT2-T_SF))*dt

        write(10,*) treel,Q,Q_elec,Q_pertes,T_evT,T_evT2

    end do

    close(10)

end program
