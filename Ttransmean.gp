reset

set terminal postscript eps enhanced color dashed
set output "T_evtrans.eps"
set size 1.,1.

set nokey
set tmargin 1
set lmargin 10
set rmargin 10

t0=0
tf=530

set xrange [0:] 
#set xrange [430:520]
#set xrange [445:495]
set xtics 0,20,1000 font "Helvetica,18,bold"  tc rgb "black" nomirror
set mxtics 2
set xlabel 'time [min]' font "Helvetica,18,bold" 

ymin=0;ymax=130
set yrange [ymin:ymax]
set ytics 0,10,130 font "Helvetica,18,bold"  tc rgb "black" nomirror
set mytics 5
set format y "%g "
set ylabel 'evaporator mean temperature [{\260}C]' font "Helvetica,18,bold" 

y2min=100;y2max=400
set y2range [y2min:]
set y2tics 0,500,50000 font "Helvetica,18,bold"  tc rgb "black"
set format y2 "%g "
set y2label 'power [W]' font "Helvetica,18,bold" 

set key right font "Helvetica,18,bold"

plot 'Sorties.out' u ($1/60):3 axes x1y2 t 'Q' w l lw 3 lc rgb "black",\
	'Sorties.out' u ($1/60):5 t 'modelling' w l lw 3 lc rgb "red",\
	'Sorties.out' u ($1/60):6 t 'modelling (m_{steel}/2)' w l lw 3 lc rgb "forest-green",\
	#'Sorties.out' u ($1/60):($7/1000) axes x1y2 t 'En' w l lw 3 lc rgb "blue",\
	
	#'H:\Mes documents\DocML\1.Experimental\3.PHP\Resultats_4mm\2021-04-29_eau_TR35_GS\Ttransmean.txt' u ($1/60-t0):(($4+$5+$6+$7+$8)/5) every 10 t 'experimental' w l lw 3 lc rgb "blue",\
	#'H:\Mes documents\DocML\1.Experimental\3.PHP\Resultats_4mm\2021-04-29_eau_TR35_GS\Ttrans.txt' u ($1/60-t0):($2+$3) every 10  axes x1y2 t 'Q_{exp}' w l lw 3  lc rgb "black",\
	
	#'Sorties.out' u ($1/60):3 t 'T_{Q,V1}' w l lw 3,\
	'Sorties.out' u ($1/60):4  t 'T_{Q,V2}' w l lw 3,\
	'Sorties.out' u ($1/60):5 t 'T_{DT,V1}' w l lw 3,\
	'Sorties.out' u ($1/60):6  t 'T_{DT,V2}' w l lw 3,\
	'Sorties.out' u ($1/60):7 t 'T_{T,V1}' w l lw 3,\
	'Sorties.out' u ($1/60):6  t 'T_{DT,V2}' w l lw 3,\
	'Sorties.out' u ($1/60):4  t 'T_{Q,V2}' w l lw 3,\

reset
