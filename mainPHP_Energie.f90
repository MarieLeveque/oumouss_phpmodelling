program main

    implicit double precision (a-h, l-z)
    !integer, parameter :: ktot!=550*60!260*60!

    pi=acos(-1.)

!--------------param�tres thermo-physique acier maraging-----------------!

    !m_bloc=0.2335+0.1802                !kg
    m_acier0=4.286                       !kg
    rho_acier=8.0e3                     !kg/m3
    m_cond=rho_acier*(pi*6e-2**2*2e-2-pi*5e-2*pi*4e-3**2)!2cm enlev�
    m_acier=m_acier0-m_cond                        !kg
    !print*,rho_acier*V_cond
    V_rmv=4.4e-2*pi*6.9e-2**2/4
    !m_acier2=m_acier-rho_acier*V_cond    !kg
    m_acier2=m_acier-rho_acier*V_rmv    !kg
    !m_acier2=m_acier/2                     !kg
    print*,'m_acier_ev =',m_acier,'kg'
    print*,'m_acier2 =',m_acier2,'kg'
    print*,'m_cond =',m_cond,'kg'

    d_php=8e-2                          !m
    h_php=4e-2                          !m
    S_iso=pi*d_php**2/4     !m2
    e_iso=4e-3                          !m

    h_atm=10
    c_acier=450                         !J/kgK (Durnico, � v�rifier)
    lambda_iso=0.04                     !W/mK

    R_iso=((lambda_iso*S_iso)/e_iso+2*pi*lambda_iso*h_php/log((d_php+2*e_iso)/d_php))**(-1)+1/(h_atm*S_iso)        !W/K
    print*,R_iso

!-------------------param�tres conductance equivalente -----------------!



    !aT=-0.000295666
    !bT=0.0618102
    !cT=-0.0784746

    !aT=-0.00029334
    !bT=0.0612515
    !cT=-0.0775118

    !aT=-0.000245633
    !bT=0.0506919
    !cT=0.518477

    aT=1.30227e-005
    bT=-0.00378628
    cT=0.370325
    dT=-9.10969

    aT=-0.000490811
    bT=0.0961139
    cT=-1.61031

	!aDT=-0.000501758
    !bDT=0.0873636
    !cDT=-0.676493

    !a=-2.8279e-005
    !b=0.0157468
    !c=0.929195

!----------------------Conditions initiales/limites --------------------!

    T_0=60
    T_SF=10         !�C
    T_ev=T_0       !�C
    T_ev2=T_0       !�C
    T_evG=T_0        !�C
    T_evG2=T_0       !�C
    T_evT=T_0       !�C
    T_evT2=T_0      !�C
    T_atm=30          !�C

    Entot=21e3         !J

    dt=1            !s pas de temps
    Nen=0

!--------------------param�tres cycle puissance ------------------------!


    !Q1= 91! 0       !W puissance initiale
    !Step=20         !W valeur du saut de puissance
    !iNS=4           !nombre de saut de puisance
    t1Q=0         !param�tres de la rampe de puissance
    t2Q=1
    !kperm=0.511*3600 !ktot/iNs!!dur�e de chaque palier

!---------------------- type de cycle --------------------!

    print*,'cycle ? PS=0 ou GS=1'
    read*,Ncycle
    if (Ncycle == 0) then
        ktot=550*60
        Q1= 91
        Step=20
        iNS=12
        kperm=0.511*3600
    elseif (Ncycle == 1) then
        ktot=260*60/2
        Q1=0
        !Step=150
        iNS=2!4
        kperm=ktot/iNs
    else !(Ncycle == 1) then
        ktot=260*60/2
        Q1=17000
        !Step=150
        iNS=1!4
        kperm=ktot/iNs
    end if

    open (unit=10,file='Sorties.out')

!-------------------------evolution transitoire-------------------------!

    do k=0,ktot

        treel=k*dt

        !cycle de puissance
        if (Ncycle == 0) then
            Q_elec=Q1+(27)/(1+exp(-((treel-(3100-60*4)*dt-kperm*dt)-t1Q)/t2Q))&
            +(Q1+Step-Q1)/(1+exp(-((treel-(3100+1200)*dt-kperm*dt*2)-t1Q)/t2Q))&
            +(18)/(1+exp(-((treel-(3100+1200*2-70)*dt-kperm*dt*3)-t1Q)/t2Q))&
            +(Q1+Step-Q1)/(1+exp(-((treel-(3100*2+1200*2-70+60)*dt-kperm*dt*4)-t1Q)/t2Q))&
            +(Q1+Step-Q1)/(1+exp(-((treel-(3100*2+1200*2+60*3)*dt-kperm*dt*5)-t1Q)/t2Q))&
            +(19)/(1+exp(-((treel-(3100*2+1200*2+60*3.5)*dt-kperm*dt*6)-t1Q)/t2Q))&
            +(18)/(1+exp(-((treel-(3100*2+1200*2+60)*dt-kperm*dt*7)-t1Q)/t2Q))&
            +(Q1+Step-Q1)/(1+exp(-((treel-(3100*2+1200*2+60)*dt-kperm*dt*8)-t1Q)/t2Q))&
            +(Q1+Step-Q1)/(1+exp(-((treel-(3100*2+1200*2+40)*dt-kperm*dt*9)-t1Q)/t2Q))&
            +(19)/(1+exp(-((treel-(3100*2+1200*2+20)*dt-kperm*dt*10)-t1Q)/t2Q))&
            -(292)/(1+exp(-((treel-(3100*2+1200*2)*dt-kperm*dt*11)-t1Q)/t2Q))!&
            !!+(Q1+Step-Q1)/(1+exp(-((treel-kperm*dt*6)-t1Q)/t2Q))&
            !+(Q1+Step-Q1)/(1+exp(-((treel-kperm*dt*7)-t1Q)/t2Q))&
            !+(Q1+Step-Q1)/(1+exp(-((treel-kperm*dt*8)-t1Q)/t2Q))
        elseif (Ncycle == 1) then
            Q_elec=Q1+300/(1+exp(-((treel-(60*5)*dt))/t2Q))&
            -300/(1+exp(-((treel-(kperm+60*5)*dt))/t2Q))
            !Q_elec=Q1+(294)/(1+exp(-((treel-60*dt)-t1Q)/t2Q))&
            !-(150)/(1+exp(-((treel-(kperm-3*60)*dt*1)-t1Q)/t2Q))&
            !+(150)/(1+exp(-((treel-(kperm-5*60)*dt*2)-t1Q)/t2Q))&
            !-(294)/(1+exp(-((treel-(kperm-4*60)*dt*3)-t1Q)/t2Q))
        else
            Q_elec=Q1-Q1/(1+exp(-(treel)/t2Q))!&
            !-300/(1+exp(-((treel-(kperm+60*5)*dt))/t2Q))
            !Q_elec=Q1+(294)/(1+exp(-((treel-60*dt)-t1Q)/t2Q))&
            !-(150)/(1+exp(-((treel-(kperm-3*60)*dt*1)-t1Q)/t2Q))&
            !+(150)/(1+exp(-((treel-(kperm-5*60)*dt*2)-t1Q)/t2Q))&
            !-(294)/(1+exp(-((treel-(kperm-4*60)*dt*3)-t1Q)/t2Q))
        endif

        Q_pertes=(T_evT-T_atm)/R_iso
        Q=Q_elec-Q_pertes
        En=Q*treel
        if (En>=Entot .AND. Nen==0) then
        print*,'En=Entot',treel/60
        Nen=1
        end if

        !conductance
        !G_eq=a*Q**2+b*Q+c     !W/K
		!G_eqDT=aDT*(T_evG-T_SF)**2+bDT*(T_evG-T_SF)+cDT
		!G_eqDT2=aDT*(T_evG2-T_SF)**2+bDT*(T_evG2-T_SF)+cDT

        if (T_evT>66) then
            G_eqT=aT*T_evT**2+bT*T_evT+cT
            !G_eqT=aT*T_evT**3+bT*T_evT**2+cT*T_evT+dT
            !G_eqT2=aT*T_evT2**3+bT*T_evT2**2+cT*T_evT2+dT
        !else if (T_evT>63.5) then
            !G_eqT=2.8!(0.2*(aT*(T_evT)**2+bT*(T_evT)+cT)+0.8*1.75)/2
            !G_eqT2=G_eqT
       else
            G_eqT=1.75
        end if

        if (T_evT2>66) then
            G_eqT2=aT*T_evT2**2+bT*T_evT2+cT
        else
            G_eqT2=G_eqT
        endif
        !evolution de la temp�rature
        !T_ev=T_ev+(Q/(m_acier*c_acier)-G_eq/(m_acier*c_acier)*(T_ev-T_SF))*dt
        !T_ev2=T_ev2+(Q/(m_acier2*c_acier)-G_eq/(m_acier2*c_acier)*(T_ev2-T_SF))*dt
		!T_evG=T_evG+(Q/(m_acier*c_acier)-G_eqDT/(m_acier*c_acier)*(T_evG-T_SF))*dt
        !T_evG2=T_evG2+(Q/(m_acier2*c_acier)-G_eqDT2/(m_acier2*c_acier)*(T_evG2-T_SF))*dt
		T_evT=T_evT+(Q/(m_acier*c_acier)-G_eqT/(m_acier*c_acier)*(T_evT-T_SF))*dt
        T_evT2=T_evT2+(Q/(m_acier2*c_acier)-G_eqT2/(m_acier2*c_acier)*(T_evT2-T_SF))*dt

        write(10,*) treel,Q,Q_pertes,Q_elec,T_evG,T_evG2,T_evT,T_evT2,En

    end do

    close(10)

end program
