clear all 
close all 

load SortiesGS.out
DE=SortiesGS;
eps=69;
eps2=0.1;
n=1;
Nt1=0;
Nt2=0;
Nt3=0;
Nt4=0;


%-----------donn�es exp�rimentales----------------

t=DE(:,1);              %s      %temps depuis le d�marrage de l'acquisition
T_pmoy=DE(:,8);            %�C     %temp�rature du bloc chauffant T1
Q=DE(:,2);              %W      %puissance inject�e dans les cartouches chauffantes
                        

%T_sfmoy=(T_sfin-T_sfout)/2  ; 
%T_pmoy=(T_p1+T_p2+T_p3)/3;


%----------d�tection des palliers--------------

for i=2:length(T_pmoy)
    dQdt(i)=(Q(i)-Q(i-1))/(t(i)-t(i-1));
    if abs(dQdt(i))>eps 
        tp(n)=t(i);
        lp(n)=i-5;
        n=n+1;
    end
end 
% tp(1)=300;
% tp(2)=4200;
% lp(1)=tp(1)-5;
% lp(2)=tp(1)-5;
%-----------diff�rence de temp�rature entre chaque palier---------

DT_100=T_pmoy(lp(3))-T_pmoy(lp(1));
DT_200=T_pmoy(length(T_pmoy))-T_pmoy(lp(3));
%DT_300=T_pmoy(lp(7))-T_pmoy(lp(5));
%DT_400=T_pmoy(length(T_pmoy))-T_pmoy(lp(7));

%-----------constante de temps tau-------------------
%temps auquel la temp�rature � atteint 63% de sa valeur en RP
T_pmoy_tau100=T_pmoy(lp(1))+0.63*DT_100;
T_pmoy_tau200=T_pmoy(lp(3))+0.63*DT_200;
%T_pmoy_tau300=T_pmoy(lp(5))+0.63*DT_300;
%T_pmoy_tau400=T_pmoy(lp(7))+0.63*DT_400;

for i=1:length(T_pmoy)
    if ((abs(T_pmoy_tau100-T_pmoy(i)))<eps2) && (t(i)<tp(3)) 
        tau_100=t(i)-tp(1);
        Nt1=1;
    end
    if ((abs(T_pmoy_tau200-T_pmoy(i)))<eps2) 
        tau_200=t(i)-tp(3);
        Nt2=1;
    end
%     if ((abs(T_pmoy_tau300-T_pmoy(i)))<eps2) && (t(i)<tp(7)) 
%         tau_300=t(i)-tp(5);
%         Nt3=1;
%     end
%     if ((abs(T_pmoy_tau400-T_pmoy(i)))<eps2) 
%         tau_400=t(i)-tp(7);
%         Nt4=1;
%     end
end

figure (1)
plot (t,dQdt)
figure (2)
plot (t,T_pmoy)









