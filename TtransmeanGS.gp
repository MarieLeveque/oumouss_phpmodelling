reset

set terminal postscript eps enhanced color dashed
set output "T_evtransGS.eps"
set size 1.,1.

set nokey
set tmargin 1
set lmargin 10
set rmargin 10

t0=0
tf=530

set xrange [0:] 
#set xrange [0:520]
#set xrange [445:495]
set xtics 0,50,1000 font "Helvetica,22,bold"  tc rgb "black" nomirror
set mxtics 5
set xlabel 'time [min]' font "Helvetica,22,bold" 

ymin=0;ymax=115
set yrange [ymin:ymax]
set ytics 0,20,130 font "Helvetica,22,bold"  tc rgb "black" nomirror
set mytics 10
set format y "%g "
set ylabel 'T_{ev} [{\260}C]' font "Helvetica,22,bold" 

y2min=0;y2max=320
set y2range [y2min:y2max]
set y2tics 0,50,500 font "Helvetica,22,bold"  tc rgb "black"
set format y2 "%g "
set my2tics 5
set y2label 'heat flux [W]' font "Helvetica,22,bold" 

set key right font "Helvetica,22,bold"

plot 'SortiesGS.out' u ($1/60):3 axes x1y2 t 'Q' w l lw 3 lc rgb "black",\
	'SortiesGS.out' u ($1/60):5 t 'full cylinder' w l lw 3 lc rgb "red",\
	'SortiesGS.out' u ($1/60):6 t 'hollow cylinder' w l lw 3 lc rgb "forest-green",\
	
	#'H:\Mes documents\DocML\1.Experimental\3.PHP\Resultats_4mm\2021-04-29_eau_TR35_GS\Ttransmean.txt' u ($1/60-t0):(($4+$5+$6+$7+$8)/5) every 10 t 'experimental' w l lw 3 lc rgb "blue",\
	
	#'H:\Mes documents\DocML\1.Experimental\3.PHP\Resultats_4mm\2021-04-29_eau_TR35_GS\Ttrans.txt' u ($1/60-t0):($2+$3) every 10  axes x1y2 t 'Q_{exp}' w l lw 3  lc rgb "black",\
	
	#'Sorties.out' u ($1/60):3 t 'T_{Q,V1}' w l lw 3,\
	'Sorties.out' u ($1/60):4  t 'T_{Q,V2}' w l lw 3,\
	'Sorties.out' u ($1/60):5 t 'T_{DT,V1}' w l lw 3,\
	'Sorties.out' u ($1/60):6  t 'T_{DT,V2}' w l lw 3,\
	'Sorties.out' u ($1/60):7 t 'T_{T,V1}' w l lw 3,\
	'Sorties.out' u ($1/60):6  t 'T_{DT,V2}' w l lw 3,\
	'Sorties.out' u ($1/60):4  t 'T_{Q,V2}' w l lw 3,\

reset
